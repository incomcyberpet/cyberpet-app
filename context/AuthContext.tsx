import axios from 'axios';
import React, { useReducer, createContext, useContext, FC, useEffect } from 'react'
import EncryptedStorage from 'react-native-encrypted-storage';
import { refreshUserRequest, loginRequest, householdsRequest } from '../util/requests';
import { Household, Pet, Task, UserData } from '../models'
import AsyncStorage from '@react-native-async-storage/async-storage';

type User = {
  email: string
}

type AuthState = {
  households: Household[]
  user: User | null
  selectedHousehold: number
  access_token: string
  refresh_token: string
}

export type AuthResponse = {
  access_token: string,
  refresh_token: string,
  expires_in: string,
  token_type: string,
  scope: string
}

export enum Actions {
  SIGN_UP = "sign_up",
  SIGN_IN = "sign_in",
  SIGN_OUT = "sign_out",
  SWITCH_HOUSEHOLD = "switch_household",
  REFRESH_TOKENS = "refresh_tokens",
}

type Action =
  { type: Actions.SIGN_IN, 
    householdResponse: Household[], 
    selectedHousehold: number,
    networkResponse: AuthResponse,
    user: User }
  | { type: Actions.SIGN_UP, networkResponse: AuthResponse }
  | { type: Actions.SIGN_OUT } 
  | { type: Actions.SWITCH_HOUSEHOLD, selectedHousehold: number }
  | { type: Actions.REFRESH_TOKENS, refreshToken: string, accessToken: string }

const initialState: AuthState = {
  user: null,
  selectedHousehold: 0,
  households: [],
  access_token: "",
  refresh_token: ""
}

const AuthStateContext = createContext<{
  user: AuthState,
  dispatch: React.Dispatch<Action>
}>({
  user: initialState,
  dispatch: () => null
});

export const useAuthState = () => {
  const ctx = useContext(AuthStateContext);

  if (ctx === undefined) {
    throw new Error("useAuthState must be used within an AuthProvider");
  }

  return ctx;
}

const AuthReducer = (previousState: AuthState, action: Action) => {
  switch (action.type) {
    case Actions.SIGN_IN:
      return {
        ...previousState,
        selectedHousehold: action.selectedHousehold,
        access_token: action.networkResponse.access_token,
        refresh_token: action.networkResponse.refresh_token,
        user: action.user,
        households: [...action.householdResponse],
      };
    case Actions.SIGN_UP: 
      return {
        ...previousState,
        user: {
          email: ""
        },
        access_token: "",
      };
    case Actions.SIGN_OUT: 
      return {
        ...initialState
      };
    case Actions.SWITCH_HOUSEHOLD:
      return {
        ...previousState,
        selectedHousehold: action.selectedHousehold
      };
    case Actions.REFRESH_TOKENS:
      return {
        ...previousState,
        refresh_token: action.refreshToken,
        access_token: action.accessToken
      };
  }
}

export const AuthProvider: FC<{}> = ({ children }) => {
  const [user, dispatch] = useReducer(AuthReducer, initialState);
  
  useEffect(() => {
    const loadUserCreds = async () => {
      try {
        console.log("AuthProvider: logging in user");

        const userEmail = await EncryptedStorage.getItem("user_email")
        const refreshToken = await EncryptedStorage.getItem("refresh_token")
        const accessToken = await EncryptedStorage.getItem("access_token")
        
        if (accessToken === undefined || refreshToken === undefined) {
          return
        } 

        if (accessToken === null || refreshToken === null) {
          return
        }

        console.log(`AuthProvider: making refresh request with access token
                    ${accessToken} and refresh token ${refreshToken}`)

        const r = await refreshUserRequest(accessToken, refreshToken);

        const i = await EncryptedStorage.setItem("access_token", r.access_token);
        console.log("setting token", i)
        await EncryptedStorage.setItem("refresh_token", r.refresh_token);

        const h = await householdsRequest(r.access_token);

        const s = Number(await AsyncStorage.getItem("selectedHousehold"));

        dispatch({
          type: Actions.SIGN_IN, 
          householdResponse: h,
          networkResponse: r, 
          selectedHousehold: s,
          user: { 
            email: userEmail || ""
          }
        })
      }
      catch (e) {
        console.log("AuthProvider failed to log in:", e)
      }
    }

    loadUserCreds();
  }, [])

  return (
    <AuthStateContext.Provider value={{ user: user, dispatch: dispatch }}>
      {children}
    </AuthStateContext.Provider>
  );
}
