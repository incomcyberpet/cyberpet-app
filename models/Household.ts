type Household = {
    id: number,
    address: string,
    name: string
}

export default Household;