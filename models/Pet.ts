type Pet = {
    id: number,
    name: string,
    breed?: string,
    age?: number,
    height?: number,
    weight?: number,
    species?: string
}

export default Pet;