type RegisterModel = {
    email: string,
    password: string,
    displayname: string
}

export default RegisterModel;