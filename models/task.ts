import Pet from './Pet'

type Task = {
  id: number,
  name: string,
  description?: string,
  notes?: string,
  completed: boolean,
  startTime?: Date,
  endTime?: Date,
  completedAt?: Date,
  enabled?: boolean,
  originTask?: number,
  assignedPet?: Pet 
}

export default Task;
