type UserTask = {
    userid: string,
    taskid: number
}

export default UserTask;