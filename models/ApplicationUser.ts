type ApplicationUser = {
    profilepictureurl: string,
    displayname: string,
    passwordhash: string,
    securitystamp: string
}

export default ApplicationUser;