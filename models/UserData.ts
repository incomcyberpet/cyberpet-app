import { Household, Task, Pet } from './'

type UserData = {
    households: Household[],
    tasks: Task[],
    pets: Pet[]
}

export default UserData;
