type HouseholdUser = {
    userid: string,
    householdid: number,
    isadmin: boolean
}

export default HouseholdUser;