type HouseholdViewModel = {
    name: string,
    address: string,
    id: number,
    ownerid: string
}

export default HouseholdViewModel;