import React from 'react';
import 'react-native-gesture-handler';
import { Typography, Colors, ThemeManager } from 'react-native-ui-lib';
import { AuthProvider } from './context/AuthContext';
import Navigation from './components/Navigation';

Typography.loadTypographies({
  h1: {
    fontSize: 52, 
    fontFamily: "Poppins",
    fontWeight: '400',
    lineHeight: 56 
  },
  h1Light: {
    fontSize: 52, 
    fontFamily: "Poppins", 
    fontWeight: '300', 
    lineHeight: 60
  },
  h2: {
    fontSize: 44, 
    fontFamily: "Poppins", 
    fontWeight: '400', 
    lineHeight: 48 
  },
  h3: {
    fontSize: 36,
    fontFamily: "Poppins",
    fontWeight: '300',
    lineHeight: 40
  },
  h4: {
    fontSize: 22,
    fontFamily: "Poppins",
    fontWeight: '300',
    lineHeight: 36 
  },
  bodyLarge: {
    fontSize: 18,
    fontFamily: "Poppins", 
    fontWeight: '400', 
    lineHeight: 22
  },
  body: { 
    fontSize: 16,
    fontFamily: "Poppins", 
    fontWeight: '400', 
    lineHeight: 20
  },
  pageTitle: {
    fontSize: 48,
    fontFamily: "Poppins",
    fontWeight: '500',
    lineHeight: 54 
  },
  subheading: {
    fontSize: 19,
    fontFamily: "Poppins",
    fontWeight: '500',
    lineHeight: 25 
  }
});

Colors.loadColors({
  primary: "#5359EF",
  primaryLight: "#c4c4ff",
  lightGray: "#c4c4c4",
  lightBackground: "#e9e9f7",
  lightText: "#f8f8f8",
})

ThemeManager.setComponentTheme('Button', {
  body: true,
});

ThemeManager.setComponentTheme('TextField', {
  body: true,
});

ThemeManager.setComponentTheme('Text', {
  fontFamily: 'Poppins'
});

declare const global: { HermesInternal: null | {} };

const App = () => { 
  return (
    <AuthProvider>
      <Navigation></Navigation>
    </AuthProvider>
  );
};

export default App;
