import React, { useEffect, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Actions, useAuthState } from '../context/AuthContext';
import Login from '../screens/Login';
import SignUp from '../screens/SignUp';
import Onboarding from '../screens/Onboarding';
import Home from '../screens/Home';
import Tasks from '../screens/Tasks';
import Settings from '../screens/Settings';
import ForgotPassword from '../screens/ForgotPassword';
import OnboardHousehold from '../screens/OnboardHousehold';
import { Text, Button } from 'react-native';
import ChooseHousehold from '../screens/ChooseHousehold';
import AsyncStorage from '@react-native-async-storage/async-storage';
import EncryptedStorage from 'react-native-encrypted-storage'
import { axiosInstance, refreshUserRequest } from '../util/requests'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const Navigation = () => {
  const { user, dispatch } = useAuthState();
  const selectedHousehold = user.selectedHousehold;

  useEffect(() => {
    const getHouseholdFromStorage = async () => {
      const h: number = Number(await AsyncStorage.getItem("selectedHousehold"));

      dispatch({
        type: Actions.SWITCH_HOUSEHOLD,
        selectedHousehold: h
      })
    }

    getHouseholdFromStorage();
  }, []);
  
  useEffect(() => {
    const requestInterceptorFunction = async (config: any) => {
      console.log("Request interceptor: setting auth token");

      const token = await EncryptedStorage.getItem("access_token");
      console.log(`Request interceptor: using token: ${token}`)

      config.headers["Authorization"] = `Bearer ${token}`

      return config;
    }

    const responseInterceptorFunction = async (error: any) => {
      console.log("401 Interceptor: running");

      const originalRequest = error.config;

      if (error.response.status == 401 && !originalRequest._retry) {
        console.log("401 Interceptor: refreshing token");

        originalRequest._retry = true;

        const accessToken = await EncryptedStorage.getItem("access_token");
        const refreshToken = await EncryptedStorage.getItem("refresh_token");

        console.log(`401 Interceptor: old access token: ${accessToken}`)
        console.log(`401 Interceptor: old refresh token: ${refreshToken}`)

        if (refreshToken && accessToken) {
          console.log("401 Interceptor: making refresh request");

          const req = await refreshUserRequest(accessToken, refreshToken);

          console.log("401 Interceptor:", req);

          const newRefreshtoken = req.refresh_token;
          const newToken = req.access_token;

          await EncryptedStorage.setItem("refresh_token", newRefreshtoken);
          await EncryptedStorage.setItem("access_token", newToken);

          console.log(`401 Interceptor: new refresh token: ${newRefreshtoken}`)
          console.log(`401 Interceptor: new access token: ${newToken}`)

          dispatch({
            type: Actions.REFRESH_TOKENS,
            refreshToken: newRefreshtoken,
            accessToken: newToken
          });
          
          return axiosInstance(originalRequest);
        }

        return error;
      }  

      console.log("401 Interceptor: Not a 401 or 403, or already retried");
    }
    const setUpAxiosInterceptors = () => {
      const requestInterceptor =
        axiosInstance.interceptors.request.use(requestInterceptorFunction, (error) => {});

      const authInterceptor = axiosInstance.interceptors.response.use((resp) =>
                                                                      resp,
                                                                      responseInterceptorFunction)

      return () => {
        axiosInstance.interceptors.request.eject(requestInterceptor);
        axiosInstance.interceptors.response.eject(authInterceptor);
      }
    }

    const x = setUpAxiosInterceptors();

    return x;
  }, [user]);

  return (
    <NavigationContainer>
      {user.access_token !== "" && user.households.length == 0 ? (
        // User is signed in, but they either 
        // a. don't have any households, or
        // b. don't have a household selected.
        <Stack.Navigator 
          screenOptions={{
            headerShown: false
          }}>
          <Stack.Screen name="OnboardHousehold" component={OnboardHousehold}></Stack.Screen>
        </Stack.Navigator>
      ) : user.access_token !== "" && user.households.length > 0 && selectedHousehold <= 0 ? (
        <Stack.Navigator 
          screenOptions={{
            headerShown: false
          }}>
          <Stack.Screen name="ChooseHousehold" component={ChooseHousehold}></Stack.Screen>
        </Stack.Navigator>
      ) : user.access_token !== "" && user.households.length > 0 && selectedHousehold > 0 ? (
        // User is signed in, and they both
        // a. have households, and 
        // b. have a household selected
        <Tab.Navigator>
          <Tab.Screen name="Home" component={Home}></Tab.Screen>
          <Tab.Screen name="Tasks" component={Tasks}></Tab.Screen>
          <Tab.Screen name="Settings" component={Settings}></Tab.Screen>
        </Tab.Navigator>
      ) : (
        // User is not signed in
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
          >
          <Stack.Screen name="Onboarding" component={Onboarding}></Stack.Screen>
          <Stack.Screen name="ForgotPassword" component={ForgotPassword}></Stack.Screen>
          <Stack.Screen name="Login" component={Login}></Stack.Screen>
          <Stack.Screen name="Signup" component={SignUp}></Stack.Screen>
        </Stack.Navigator>
      )}
    </NavigationContainer>
  )
}

export default Navigation
