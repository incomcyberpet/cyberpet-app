import React from 'react'
import { TextField, Colors } from 'react-native-ui-lib';
import { TextFieldProps } from 'react-native-ui-lib/typings';

type CyberTextFieldProps = {
  hidden: boolean
}

const CyberTextField = (props: TextFieldProps | CyberTextFieldProps) => {
  const style = {
    backgroundColor: Colors.white,
    borderColor: Colors.primary,
    borderRadius: 10,
    borderWidth: 2,
    paddingTop: 10,
    paddingLeft: 15,
    paddingBottom: 10,
    paddingRight: 15,
  }

  return (
    <TextField
      titleStyle={{
        marginTop: -10
      }}
      hideUnderline
      style={style}
      placeholderTextColor="grey"
      autoCapitalize="none"
      {...props}
    />

  );
}

export default CyberTextField;
