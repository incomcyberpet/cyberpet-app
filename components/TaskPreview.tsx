import React from 'react';
import { TouchableOpacity } from 'react-native'
import { View, Text, Colors, Button } from 'react-native-ui-lib';
import Task from '../models/task';

interface TaskPreviewProps {
  task: Task
}

const TaskPreview: React.FC<TaskPreviewProps> = (props) => {
  const { task } = props;

  return (
    <View paddingT-10 paddingB-15 paddingH-15 marginB-15 style={{
      borderWidth: 2,
      borderRadius: 10,
      borderColor: Colors.lightGray
    }}>

      <View row spread>

        <Text style={{
          fontSize: 24,
          fontWeight: '600',
          fontFamily: "Poppins"
        }}>{task.name}</Text>
      
        <TouchableOpacity
          padding-0 
          style={{
            width: 45,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            height: 25,
            padding: 0,
            paddingLeft: 10,
            paddingRight: 10,
            borderRadius: 5,
            backgroundColor: Colors.primaryLight
          }}
        >
          <View style={{
            borderRadius: 5,
            width: 7,
            height: 7,
            backgroundColor: Colors.white,
          }}></View>
          <View style={{
            borderRadius: 5,
            width: 7,
            height: 7,
            backgroundColor: Colors.white,
          }}></View>
          <View style={{
            borderRadius: 5,
            width: 7,
            height: 7,
            backgroundColor: Colors.white,
          }}></View>
        </TouchableOpacity>

      </View>

      <View>
        {task.startTime && (
          <Text marginT-5 style={{
            fontSize: 16,
            fontWeight: "600"
          }}>{new Date(task.startTime).toDateString()}</Text>
        )}

        <Text body marginT-10>{task.description}</Text>

      </View>


    </View>
  )
}

export default TaskPreview;
