import { useNavigation, useRoute } from '@react-navigation/core';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs'
import React, { ReactHTML } from 'react';
import { ScrollView, ViewStyle, ScrollViewProps } from 'react-native'
import { Text, View, Colors } from 'react-native-ui-lib';

type ScreenWrapperProps = {
  style?: ViewStyle,
  scroll?: boolean,
  refreshControl?: React.ReactElement | string
}

const ScreenWrapper: React.FC<ScreenWrapperProps> = ({ 
  children, 
  scroll, 
  style,
  refreshControl 
}) => {
  const route = useRoute();
  const tabHeight = useBottomTabBarHeight();
  
  return (
    <View 
      backgroundColor={Colors.grey80} 
      style={{ height: "100%"}} >

      <View 
        paddingT-75 
        paddingB-20 
        paddingH-30 
        backgroundColor={Colors.primary}>

        <Text pageTitle style={{
          color: Colors.white
          }}>{route.name}</Text>
        
      </View>

      {scroll ? (
        <ScrollView style={{ 
            paddingLeft: 30,
            paddingRight: 30,
          }} 
          refreshControl={refreshControl}
          flex-1>
          {children}
        </ScrollView>
      ) : (
        <View flex-1 style={{
          paddingLeft: 30,
          paddingRight: 30
          }}> 
          {children}
        </View> 
      )}
    </View>
  )
}

export default ScreenWrapper;
