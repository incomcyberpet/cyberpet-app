import React, { useState } from 'react'
import { Carousel, PageControl, Text, View } from 'react-native-ui-lib';
import { ViewProps } from 'react-native-ui-lib/generatedTypes/components/view';

interface WelcomePage {
  title: string;
  description: string;
}

const welcomePages : WelcomePage[] = [
  {
    title: "Manage your pets!",
    description: "Test description"
  },
  {
    title: "Assign tasks",
    description: "Test description"
  },
  {
    title: "Test",
    description: "Test description"
  }
]

const WelcomeCarousel = (viewProps: ViewProps) => {
  const [currentPage, setCurrentPage] = useState(0)

  return (
    <View {...viewProps}>
      <Carousel onChangePage={(i, o) => setCurrentPage(i)}>
        {welcomePages.map((item, index) => (
          <View height={150} marginH-35 key={item.title}>
            <Text h3>{item.title}</Text>
            <Text>{item.description}</Text>
          </View>
        ))}
      </Carousel>
      <PageControl currentPage={currentPage} numOfPages={welcomePages.length}></PageControl>
    </View>
  )
}

export default WelcomeCarousel;
