import React, { useEffect, useState } from 'react'
import { StyleProp, ViewStyle } from 'react-native'
import { Task, Pet } from '../models';
import { Colors, Button, View, Text } from 'react-native-ui-lib';
import CyberTextField from '../components/TextField'
import CyberButton from '../components/Button'
import { Formik } from 'formik';
import { taskDetailRequest } from '../util/requests';
import { useAuthState } from '../context/AuthContext';

type TaskModalContentProps = {
  task: Task
}

const TaskModalContent: React.FC<TaskModalContentProps> = (props: TaskModalContentProps) => {
  const { task } = props;
  const {
    completed,
    completedAt,
    description,
    enabled,
    endTime,
    id,
    name,
    notes,
    originTask,
    startTime,
  } = task;

  const [editMode, setEditMode] = useState(false);
  const [taskCompleted, setCompleted] = useState(completed);
  const [assignedPet, setAssignedPet] = useState<Pet>();
  
  const { user, dispatch } = useAuthState();

  const sectionStyle: StyleProp<ViewStyle> = {
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: Colors.lightGray
  }

  useEffect(() => {
    const getTaskDetails = async () => {
      let x = await taskDetailRequest(user.selectedHousehold, id);

      if (x != null) {
        setAssignedPet(x.assignedPet);
      }
    }

    getTaskDetails();
  }, [])

  return (
    <View paddingH-25>
      <View row spread >
        {!editMode ? 
          <CyberButton 
            outline 
            labelStyle={{
              lineHeight: 22,
              margin: 0
            }}
            onPress={() => setEditMode(true)}
            size={Button.sizes.small}
            label={`✍️`} />
          : 
          <CyberButton 
            size={Button.sizes.small}
            labelStyle={{
              lineHeight: 22,
              margin: 0
            }}
            onPress={() => setEditMode(false)}
            label={`Save`} />
        }
        <CyberButton
          onPress={() => {
            if (taskCompleted)
              setCompleted(false); 
            else 
              setCompleted(true);
          }}
          size={Button.sizes.small}
          body
          labelStyle={{
            lineHeight: 22,
            margin: 0
          }}
          outline={taskCompleted}
          label={taskCompleted ? "🎉  Finished" : "Finish task"}
          />
      </View>

      <View marginT-25 row spread >
        <Text h2>{name}</Text>
      </View>

      {startTime && (
        <View marginT-5>
          <Text body>Due {new Date(startTime).toDateString()}</Text>
        </View>
      )}

      {description && (
        <View marginT-15>
          <Text bodyLarge>{task.description}</Text>
        </View>
      )}

      {notes && ( 
        <View marginT-15>
          <Text subheading marginB-5>Additional notes</Text>
          <Text body>{notes}</Text>
        </View>
      )}

      {completed && completedAt && (
        <View marginT-15>
          <Text 
            bodyLarge 
            style={{
              color: Colors.green10
            }}>Completed {new Date(completedAt).toDateString()}</Text>
        </View>
      )}

      {assignedPet && (
        <View marginT-25>
          <Text subheading marginB-5>Assigned to</Text>
          <Text bodyLarge>{assignedPet.name}</Text>
        </View>
      )}
    </View>
  );
}
 
export default TaskModalContent;
