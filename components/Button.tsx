import React from 'react'
import { Button, ButtonPropTypes } from 'react-native-ui-lib'

const CyberButton = (props: ButtonPropTypes) => {

  return (
    <Button
      borderRadius={10}
      enableShadow
      outlineWidth={props.outline ? 2 : undefined}
      {...props}
    >
    </Button>
  );
}

export default CyberButton;
