import axios, { AxiosResponse, AxiosError } from 'axios'
import { Household, Pet, Task } from '../models'
import { AuthResponse } from '../context/AuthContext';
import EncryptedStorage from 'react-native-encrypted-storage';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Actions, useAuthState } from '../context/AuthContext';

const baseUrl = "https://406caadd8e91.ngrok.io"

export const axiosInstance = axios.create();

export const signupRequest = async (name: string, email: string, password: string) => {
  let x = await axios.post(`${baseUrl}/api/auth/register`, {
    email: email,
    password: password,
    displayName: name,
  });

  return x.data
}

export const loginRequest = async (email: string, password: string):
  Promise<AuthResponse> => {
  let params = new URLSearchParams();

  params.append("grant_type", "password");
  params.append("username", email);
  params.append("password", password);
  params.append("scope", "openid profile offline_access");
  params.append("client_id", "mobile_app");
  params.append("client_secret", "secret");

  let config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  }

  let x = await axios.post(`${baseUrl}/connect/token`, params, config);

  console.log("LoginRequest:", x.data);

  return x.data
}

export const refreshUserRequest = async (accessToken: string, refreshToken:
                                         string): Promise<AuthResponse> => {
  let params = new URLSearchParams();

  params.append("grant_type", "refresh_token");
  params.append("client_id", "mobile_app");
  params.append("client_secret", "secret");
  params.append("refresh_token", refreshToken);

  let config = {
    headers: {
      "Authorization": `Bearer ${accessToken}`,
      "Content-Type": "application/x-www-form-urlencoded"
    }
  }

  let req = await axios.post(`${baseUrl}/connect/token`, params, config);

  return req.data;
}

export const householdsRequest = async (token: string | null): Promise<Household[]> => {
  console.log("HouseholdsRequest: getting user households");

  try {
    let h;

    if (token == null) {
      h = await axiosInstance.get(`${baseUrl}/api/households`);
    } else {
      h = await axios.get(`${baseUrl}/api/households`, {
        headers: {
          Authorization: `Bearer ${token}`
        }          
      });
    }
    console.log("HouseholdsRequest: household data", h.data);

    return h.data;
  } catch (e) {
    console.log("HouseholdsRequest: Failed to get households");
  }

  return []
}

export const petsRequest = async (householdId: number, token: string):
  Promise<Pet[]> => {

  console.log(`PetsRequest: Getting pets for household ${householdId}`);
  
  try {
    let p = await axiosInstance.get(`${baseUrl}/api/households/${householdId}/pets`);
    console.log("PetsRequest:", p.data);

    return p.data;
  } catch (e) {
    console.log("PetsRequest: Failed to get pets");
  }

  return [];
}


export const taskDetailRequest = async (householdId: number, taskId: number):
  Promise<Task | null> => {

  console.log(`TaskRequest: Getting task ${taskId} for household ${householdId}`);
  
  try {
    let p = await
    axiosInstance.get(`${baseUrl}/api/households/${householdId}/tasks/${taskId}`);
    console.log("TaskRequest:", p, p.data);

    return p.data;
  } catch (e) {
    console.log("TaskRequest: Failed to get tasks:", e);
  }

  return null;
}

export const tasksRequest = async (householdId: number):
  Promise<Task[]> => {

  console.log(`TasksRequest: Getting tasks for household ${householdId}`);
  
  try {
    let p = await axiosInstance.get(`${baseUrl}/api/households/${householdId}/tasks`);
    console.log("TasksRequest:", p, p.data);

    return p.data;
  } catch (e) {
    console.log("TasksRequest: Failed to get tasks:", e);
  }

  return [];
}
