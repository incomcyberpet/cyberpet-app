import React, { useCallback, useMemo, useRef, useState, useEffect } from 'react'
import { Button, View, Text } from 'react-native-ui-lib';
import { ScrollView, Pressable, RefreshControl } from 'react-native'
import ScreenWrapper from '../components/ScreenWrapper';
import TaskPreview from '../components/TaskPreview';
import BottomModalBackdrop from '../components/BottomModalBackdrop';
import { useAuthState } from '../context/AuthContext';
import Task from '../models/task';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs'
import { tasksRequest } from '../util/requests';
import { BottomSheetModal, BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import TaskModalContent from '../components/TaskModalContent';

const wait = (timeout: any) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const Tasks = () => {
  const { user, dispatch } = useAuthState();

  const [tasks, setTasks] = useState<Task[]>([]);
  const [completedTasks, setCompletedTasks] = useState<Task[]>([]);
  const [selectedTask, setSelectedTask] = useState<Task | null>(null);

  const tabHeight = useBottomTabBarHeight();

  useEffect(() => {
    const getTasks = async () => {
      let t = await tasksRequest(user.selectedHousehold);

      setTasks(t.filter(t => !t.completed));
      setCompletedTasks(t.filter(t => t.completed));
    }

    getTasks();
  }, [user.selectedHousehold]);


  const bottomSheetModalRef = useRef<BottomSheetModal>(null);
  const snapPoints = useMemo(() => ['85%'], []);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);

    const t = await tasksRequest(user.selectedHousehold);

    setTasks(t.filter(t => !t.completed));
    setCompletedTasks(t.filter(t => t.completed));

    setRefreshing(false);
  }, []);

  return (
    <BottomSheetModalProvider>
      <ScreenWrapper 
        scroll
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          /> 
        }>
        <Text h3 marginT-30 marginB-20 style={{fontWeight: "600"}}>Today's tasks</Text>

        {tasks.length > 0 ? tasks.map((t) => (
          <Pressable
            key={t.id}
            onPress={() => {
              setSelectedTask(t);
              bottomSheetModalRef.current?.present();
            }}>
            <TaskPreview task={t} ></TaskPreview>
          </Pressable>
        )) : (
          <Text body>Yay! No tasks due today</Text>
        )}

        {completedTasks.length > 0 && (
          <>
            <Text h3 marginT-30 marginB-20 style={{fontWeight: "600"}}>Completed tasks</Text>

            {completedTasks.map((t) => (
              <Pressable
                key={t.id}
                onPress={() => {
                  setSelectedTask(t);
                  bottomSheetModalRef.current?.present();
                }}>
                <TaskPreview task={t} key={t.id}></TaskPreview>
              </Pressable>
            ))}
          </>
        )}
      </ScreenWrapper>

        <BottomSheetModal
          style={{
            shadowColor: "#444",
            shadowOffset: {
              width: 0,
              height: -8,
            },
            shadowOpacity: 0.25,
            shadowRadius: 15,
            elevation: 16,
          }}
          backdropComponent={BottomModalBackdrop}
          onDismiss={() => {
            setSelectedTask(null);
          }}
          ref={bottomSheetModalRef}
          index={0}
          snapPoints={snapPoints}>

          <TaskModalContent task={selectedTask}></TaskModalContent>
        </BottomSheetModal>
    </BottomSheetModalProvider>
  )
}

export default Tasks;
