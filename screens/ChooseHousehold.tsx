import React, { useEffect, useState } from 'react'
import EncryptedStorage from 'react-native-encrypted-storage';
import { TouchableOpacity, Colors, Text, Button, View } from 'react-native-ui-lib';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import { Actions, useAuthState } from '../context/AuthContext'
import CyberButton from '../components/Button'

const ChooseHousehold = () => {
  const { user, dispatch } = useAuthState();
  const navigation = useNavigation();

  return (
    <View marginH-30 marginT-100>
      <Text h1 marginB-50>Choose household</Text>
      
      {user.households.map((h) => (
        <TouchableOpacity 
          key={h.id}
          backgroundColor={Colors.blue40} 
          paddingT-10
          paddingH-10
          marginB-25
          onPress={async () => {
            await AsyncStorage.setItem("selectedHousehold", "" + h.id);
            dispatch({
              type: Actions.SWITCH_HOUSEHOLD,
              selectedHousehold: h.id
            }) 
          }}>
          <Text h3>
            {h.name} 
          </Text>
        </TouchableOpacity>
      ))}
      
      <CyberButton
        outline
        label="Sign out"
        marginT-10
        marginL-0
        onPress={async () => {
          await AsyncStorage.removeItem("selectedHousehold");
          await EncryptedStorage.removeItem("user_password");
          dispatch({ type: Actions.SIGN_OUT });
        }} />
    </View>
  )
}

export default ChooseHousehold;
