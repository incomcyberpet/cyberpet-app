import React from 'react'
import { View, Text } from 'react-native-ui-lib';
import ScreenWrapper from '../components/ScreenWrapper';
import { useAuthState } from '../context/AuthContext';

const Home = () => {
  const { user, dispatch } = useAuthState();

  return (
    <ScreenWrapper>
      {user.user?.email && (
        <Text marginT-10>{user.user.email}</Text>
      )}
      {user.selectedHousehold && (
        <Text marginT-10>{user.selectedHousehold}</Text>
      )} 
      {user.refresh_token && (
        <Text marginT-10>{user.refresh_token}</Text>
      )}
      {user.access_token && (
        <Text marginT-10>{user.access_token}</Text>
      )}
    </ScreenWrapper>
   )
}

export default Home;
