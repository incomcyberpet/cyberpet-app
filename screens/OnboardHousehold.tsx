import React, { useEffect, useState } from 'react'
import EncryptedStorage from 'react-native-encrypted-storage';
import { Text, Button, View } from 'react-native-ui-lib';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import { Actions, useAuthState } from '../context/AuthContext'

const OnboardHousehold: React.FC<{}> = () => {
  const [selectedHousehold, setSelectedHousehold] = useState(0);
  const nav = useNavigation();
  const { user, dispatch } = useAuthState();

  useEffect(() => {
    const getCurrentHousehold = async () => {
      const h: number = Number(await AsyncStorage.getItem("selectedHousehold"));
      
      if (h > 0) {
        dispatch({
          type: Actions.SWITCH_HOUSEHOLD,
          selectedHousehold: h
        })

        return;
      }

      if (user.households.length > 0) {
        nav.navigate("ChooseHousehold");
      }

      setSelectedHousehold(h);
    }

    getCurrentHousehold();
  }, [])

  return (
    <View marginT-100>
      <Text>You are not a part of any households!</Text>
      <Button onPress={async () => {
        await AsyncStorage.removeItem("selectedHousehold");
        await EncryptedStorage.removeItem("user_password");
        dispatch({ type: Actions.SIGN_OUT });
      }}></Button>
    </View>
  )
}

export default OnboardHousehold;
