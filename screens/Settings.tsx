import React, { useState } from 'react'
import EncryptedStorage from 'react-native-encrypted-storage';
import { View, Text, Button, Colors, Picker } from 'react-native-ui-lib';
import ScreenWrapper from '../components/ScreenWrapper';
import { Actions, useAuthState } from '../context/AuthContext';
import { Household } from '../models'
import AsyncStorage from '@react-native-async-storage/async-storage';

const Settings = () => {
  const { user, dispatch } = useAuthState();
  const [newHousehold, setNewHousehold] = useState<Household | null>(null);

  return (
    <ScreenWrapper>
      <Text marginT-25>Switch household</Text>
      <Picker 
        marginT-10
        value={newHousehold}
        onChange={async (i: any) => {
          console.log("hey")
          setNewHousehold(i);

          await AsyncStorage.setItem("selectedHousehold", "" + i.value);

          dispatch({
            type: Actions.SWITCH_HOUSEHOLD,
            selectedHousehold: i.value
          })
        }}
        placeholder="Switch household">
        {user.households.map(h => (
          <Picker.Item onPress={() => console.log("p")} key={h.id} value={h.id} label={h.name}/>
        ))}
      </Picker>


      <Button marginT-10 outlineColor={Colors.red} outline outlineWidth={3} color={Colors.lightText} onPress={async () => {
        await EncryptedStorage.removeItem("refresh_token")
        await EncryptedStorage.removeItem("access_token")
        dispatch({type: Actions.SIGN_OUT})
      }}>
        <Text>Sign out</Text>
      </Button>

    </ScreenWrapper>
  )
}

export default Settings;
