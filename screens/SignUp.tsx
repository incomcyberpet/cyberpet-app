import { useNavigation } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import {
  StatusBar
} from 'react-native';
import EncryptedStorage from 'react-native-encrypted-storage';
import { TextInput } from 'react-native-gesture-handler';
import { Text, Colors, View, Button, TextField, ColorSwatch } from 'react-native-ui-lib';
import CyberTextField from '../components/TextField'
import CyberButton from '../components/Button'
import { Actions, useAuthState } from '../context/AuthContext';
import { loginRequest, signupRequest } from '../util/requests'

const SignUp = () => {
  const navigation = useNavigation();

  const [email, setEmail] = useState("")
  const [name, setName] = useState("")
  const [password, setPassword] = useState("")

  const { user, dispatch } = useAuthState();

  return (
    <View style={{ paddingTop: 100, paddingBottom: 50, height: "100%", backgroundColor: Colors.grey70 }}>
      <StatusBar barStyle="dark-content"></StatusBar>

      <View paddingH-50 style={{ justifyContent: "space-between", height: "100%" }}>
        <View>
          <Text h2>Create an account</Text>
        </View>

        <View>
          <CyberTextField
            title="Your name"
            placeholder="Your name"
            value={name}
            onChangeText={setName}
            />

          <CyberTextField
            title="Email"
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
            />

          <CyberTextField
            title="Password"
            placeholder="Password"
            secureTextEntry
            value={password}
            onChangeText={setPassword}
            />
        </View>
          
        <View>
          <CyberButton
            marginB-15
            marginH-40
            label="Sign up"
            onPress={async () => {
              try {
                let x = await signupRequest(name, email, password);
                let loginReq = await loginRequest(email, password);

                await EncryptedStorage.setItem("user_email", email);
                await EncryptedStorage.setItem("user_password", password);

                dispatch({
                  type: Actions.SIGN_IN, 
                  householdResponse: [],
                  selectedHousehold: 0,
                  networkResponse: loginReq, 
                  user: {
                    email: email
                  }
                })
              } catch (e) {
                console.log("signup failed")
              }
            }} 
            style={{ height: 50 }}
          />

          <CyberButton
            borderRadius={10}
            size={Button.sizes.medium}
            label="Go back"
            outline
            marginH-40
            marginB-15
            onPress={() => {
              navigation.goBack()
            }} />
        </View>

      </View>
    </View>
  );
};

export default SignUp;
