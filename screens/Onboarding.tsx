import React from 'react'
import { StatusBar } from 'react-native'
import { useNavigation } from '@react-navigation/core';
import { Colors, Text, View, Button } from 'react-native-ui-lib';
import WelcomeCarousel from '../components/WelcomeCarousel';

const Onboarding = () => {
  const navigation = useNavigation();

  return (
    <View style={{ 
      height: "100%", 
      backgroundColor: Colors.grey70,
      justifyContent: "center" 
    }}>

      <StatusBar barStyle="dark-content" />

      <Text 
        h1
        marginL-35
        marginB-50 
        marginT-100>
        CyberPet
      </Text>

      <WelcomeCarousel 
        marginB-20></WelcomeCarousel>

      <View marginH-30>
        <Button 
          style={{
            height: 50
          }} 
          enableShadow
          borderRadius={10}
          marginT-20
          label="Get started"
          marginB-15
          onPress={() => {
            navigation.navigate("Signup")
          }} >
       </Button>

        <Button
          borderRadius={10}
          label="Login"
          outline
          outlineWidth={2}
          onPress={() => {
            navigation.navigate("Login")
          }}
          >
        </Button>

      </View>
    </View>
  )
}

export default Onboarding;
