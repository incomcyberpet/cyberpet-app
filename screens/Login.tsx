import { useNavigation } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import {
  StatusBar, 
} from 'react-native';
import EncryptedStorage from 'react-native-encrypted-storage';
import { TouchableOpacity, TextField, Text, Colors, View, Button } from 'react-native-ui-lib';
import { Actions, useAuthState } from '../context/AuthContext';
import { loginRequest, householdsRequest } from '../util/requests'
import CyberTextField from '../components/TextField'

const Login = () => {
  const navigation = useNavigation();

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const { user, dispatch } = useAuthState();

  useEffect(() => {
    const loadUserEmail = async () => {
      let x = await EncryptedStorage.getItem("user_email");
      
      if (x !== undefined && x !== null) {
        setEmail(x)
      }
    }

    loadUserEmail()
  }, [])

  return (
    <View style={{ 
      paddingTop: 100,
      paddingBottom: 50,
      height: "100%", 
      backgroundColor: Colors.grey70 
    }}>
      <StatusBar barStyle="dark-content"></StatusBar>

      <View style={{ height: "100%", justifyContent: "space-between" }} >
        <View paddingH-50>
          <Text h1>Login to Cyberpet</Text>
        </View>

        <View paddingH-50>
          <CyberTextField
            title="Email"
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
            />

          <CyberTextField
            title="Password"
            placeholder="Password"
            secureTextEntry
            value={password}
            onChangeText={setPassword}
            />

          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ForgotPassword");
            }}
            style={{
              paddingBottom: 5,
              alignSelf: "center",
              borderBottomWidth: 3,
              borderBottomColor: Colors.primary,
            }} 
          >
            <Text primary style= {{ fontSize: 16, fontWeight: '500' }} >
              Forgot password?
            </Text>
          </TouchableOpacity>

        </View>

        <View paddingH-50 marginT-15>
          <Button 
            style={{ height: 50 }}
            enableShadow
            borderRadius={10}
            label="Log in"
            marginB-15
            marginH-40
            onPress={async () => {
              try {
                let x = await loginRequest(email, password);
                let h = await householdsRequest(x.access_token);

                await EncryptedStorage.setItem("user_email", email);
                await EncryptedStorage.setItem("refresh_token", x.refresh_token);
                await EncryptedStorage.setItem("access_token", x.access_token);

                dispatch({
                  selectedHousehold: 0,
                  type: Actions.SIGN_IN, 
                  householdResponse: h,
                  networkResponse: x, 
                  user: {
                    email: email
                  }
                })
              } catch (e) {
                console.log("login failed")
              }
            }} >
          </Button>

          <Button
            marginH-40
            borderRadius={10}
            onPress={() => {
              navigation.goBack();
            }}
            label="Go back"
            size={Button.sizes.medium}
            outline
            outlineWidth={2}
            fullWidth={false}
            >
          </Button>
        </View>

      </View>
    </View>
  );
};

export default Login;
