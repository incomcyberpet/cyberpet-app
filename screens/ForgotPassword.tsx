import React, { useState} from 'react'
import { 
  StatusBar,
} from 'react-native'
import { useAuthState } from '../context/AuthContext'
import { 
  Colors,
  Button,
  Text,
  View,
  TextField
} from 'react-native-ui-lib'
import { useNavigation } from '@react-navigation/core'

const ForgotPassword = () => {
  const { user } = useAuthState();
  const navigation = useNavigation();
  const [email, setEmail] = useState("");

  return (
    <View style={{ 
      paddingTop: 100,
      paddingBottom: 50,
      height: "100%", 
      backgroundColor: Colors.grey70 
    }}>
      <StatusBar barStyle="dark-content" />
      
      <View style={{
        height: "100%",
        justifyContent: "space-between"  
      }}>

        <View paddingH-50>
          <Text h1>Reset your password</Text>
        </View>

        <View paddingH-50>

          <TextField
            title="Email"
            titleStyle={{
              marginTop: -10
            }}
            hideUnderline
            style={{
              backgroundColor: Colors.white,
              borderColor: Colors.primary,
              borderRadius: 10,
              borderWidth: 2,
              paddingTop: 10,
              paddingLeft: 15,
              paddingBottom: 10,
              paddingRight: 15,
            }}
            placeholder="Email"
            placeholderTextColor="grey"
            autoCapitalize="none"
            value={email}
            onChangeText={setEmail}
            />

          <Button
            borderRadius={10}
            enableShadow
            marginB-15
            marginH-40
            label="Submit">
          </Button>
        </View>


        <View paddingH-50 marginT-15>
          <Button
            onPress={() => {
              navigation.goBack();
            }}
            size={Button.sizes.medium}
            outline
            outlineWidth={2}
            marginH-40
            borderRadius={10}
            label="Go back">
          </Button>
        </View>

      </View>
    </View>
  )
}

export default ForgotPassword;
